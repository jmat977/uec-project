# FPGA Oscilloscope with FFT - UEC2 Project

<p align="center">
 <b>Created by Jakub Maternowski & Rafał Ziobrowski for UEC2 class</b>
</p>
<p align="center">
 AGH University of Science and Technology in Cracow
</p>
 <p align="center">
Microelectronics in Industry and Medicine 2018/2019
</p>

## Project specifiaction

1. Project implemented on Xilinx Basys3 board
2. Written in Vivado 2019.1
3. Uses internal 12-bit ADC and a proprietary signal conditioning board
4. The signal conditioning board was designed in Altium 16.1
5. Input voltage range: -0.5V to 0.5V
6. Frequency range: 0.1Hz to 20000Hz


## Creating Vivado project
1. Clone this repository into your working directory:
```
git clone https://gitlab.com/jmat977/uec-project.git
```
2. Open Vivado 2019.1
3. Open Tcl Console (Window -> Tcl Console or Ctrl+Shift+T)
4. Navigate to directory "vivado" inside your working directory for example:
```
cd C:/Your/path/to/working/directory/vivado
```
5. Insert and run command:
```
source ./Basys3_Oscilloscope_FFT.tcl
```

6. Running the simulation

Running simulation requires setting an absotute path to `stimulus.txt` file in the XADC wizard. The provided file in ``` src/simulation``` contains a 200Hz sinewave.

## Gallery

Basys 3 with attached signal conditioning board
<div style="text-align:center"><img src="https://gitlab.com/jmat977/uec-project/raw/master/basys3_w_sig_cond.jpg" /></div>    


Multiple cycles of ~500Hz squarewave signal and it's spectrum
<div style="text-align:center"><img src="https://gitlab.com/jmat977/uec-project/raw/master/screen_example.jpg" /></div>

## Video

[![](https://i.imgur.com/qvHxXfM.png)](https://drive.google.com/file/d/1UQAY4q7hxKs3nF4RJsM3hHYQCaRgtTIL/preview)

