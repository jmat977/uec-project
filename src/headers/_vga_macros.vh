`ifndef _vga_macros
`define _vga_macros

//bus sizes
`define VGA_BUS_SIZE_NO_RGB 26
`define VGA_BUS_SIZE        38
`define VGA_RGB_SIZE        12
`define VGA_VCOUNT_SIZE     11
`define VGA_HCOUNT_SIZE     11

//VGA bus components
`define VGA_RGB_BITS    37:26
`define VGA_VSYNC_BITS  25
`define VGA_HSYNC_BITS  24
`define VGA_VBLNK_BITS  23
`define VGA_HBLNK_BITS  22
`define VGA_VCOUNT_BITS 21:11
`define VGA_HCOUNT_BITS 10:0

//VGA bus split at input port NO RGB
`define VGA_SPLIT_INPUT_NO_RGB(BUS_NAME) \
    wire vsync_in = BUS_NAME[`VGA_VSYNC_BITS]; \
    wire hsync_in = BUS_NAME[`VGA_HSYNC_BITS]; \
    wire vblnk_in = BUS_NAME[`VGA_VBLNK_BITS]; \
    wire hblnk_in = BUS_NAME[`VGA_HBLNK_BITS]; \
    wire [`VGA_VCOUNT_SIZE-1:0] vcount_in = BUS_NAME[`VGA_VCOUNT_BITS]; \
    wire [`VGA_HCOUNT_SIZE-1:0] hcount_in = BUS_NAME[`VGA_HCOUNT_BITS];
    
//VGA bus split at input port
`define VGA_SPLIT_INPUT(BUS_NAME) \
    `VGA_SPLIT_INPUT_NO_RGB(BUS_NAME) \
    wire [`VGA_RGB_SIZE-1:0] rgb_in = BUS_NAME[`VGA_RGB_BITS];
    
//VGA bus output variables NO RGB
`define VGA_OUT_REG_NO_RGB \
    reg vsync_out; \
    reg hsync_out; \
    reg vblnk_out; \
    reg hblnk_out; \
    reg [`VGA_VCOUNT_SIZE-1:0] vcount_out; \
    reg [`VGA_HCOUNT_SIZE-1:0] hcount_out;
    
//VGA bus output variables
`define VGA_OUT_REG \
    `VGA_OUT_REG_NO_RGB \
    reg [`VGA_RGB_SIZE-1:0] rgb_out;

//VGA bus merge at output NO RGB
`define VGA_MERGE_OUTPUT_NO_RGB(BUS_NAME) \
    assign BUS_NAME[`VGA_VSYNC_BITS] = vsync_out; \
    assign BUS_NAME[`VGA_HSYNC_BITS] = hsync_out; \
    assign BUS_NAME[`VGA_VBLNK_BITS] = vblnk_out; \
    assign BUS_NAME[`VGA_HBLNK_BITS] = hblnk_out; \
    assign BUS_NAME[`VGA_VCOUNT_BITS] = vcount_out; \
    assign BUS_NAME[`VGA_HCOUNT_BITS] = hcount_out;
        
//VGA bus merge at output
`define VGA_MERGE_OUTPUT(BUS_NAME) \
    `VGA_MERGE_OUTPUT_NO_RGB(BUS_NAME) \
    assign BUS_NAME[`VGA_RGB_BITS] = rgb_out;
 
 `endif