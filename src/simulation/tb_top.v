`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/31/2019 03:20:15 AM
// Design Name: 
// Module Name: tb_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_top();
localparam CLK_PER_NS = 10;

reg tb_clk = 0, tb_rst = 0, tb_frame_start = 0;
/*
always
begin
    tb_clk = #(CLK_PER_NS/2) ~tb_clk;
end
*/
always 
begin 
 tb_clk = 1'b1; 
 #(CLK_PER_NS/2) tb_clk = 1'b0; 
 #(CLK_PER_NS/2); 
end

initial
begin
    tb_frame_start = 1'b0;
    #(CLK_PER_NS*20);
    tb_frame_start = 1'b1;
    #(CLK_PER_NS);
    tb_frame_start = 1'b0;
    //#(CLK_PER_NS*50000);
    //$finish; 
end


initial
begin
    tb_rst = 1'b1;
    #(CLK_PER_NS*10);
    tb_rst = 1'b0;
    //#(CLK_PER_NS*50000);
    //$finish; 
end


top DUT(
    .clk(tb_clk),
    .rst(tb_rst),
    .new_frame(tb_frame_start),
    .sw(12'b001111111111)
    
);

endmodule
