`timescale 1ns / 1ps

module char_rom(
    input wire pclk,
    input wire rst,
    input wire [19:0] period,
    input wire [3:0] char_xy,        
    input wire [3:0] char_xy_fft, 
    output reg [6:0] char_code,
    output reg [6:0] char_code_fft
    );  
    
    localparam 
        T_05_S_DIV   = 20'd634766,
        T_02_S_DIV   = 20'd253906,
        T_01_S_DIV   = 20'd126953,
        T_50_MS_DIV  = 20'd63477,
        T_20_MS_DIV  = 20'd25391,
        T_10_MS_DIV  = 20'd12695,
        T_5_MS_DIV   = 20'd6348,
        T_2_MS_DIV   = 20'd2539,
        T_1_MS_DIV   = 20'd1269,
        T_05_MS_DIV  = 20'd635,
        T_02_MS_DIV  = 20'd254,
        T_01_MS_DIV  = 20'd127;
    
    wire [6:0] addr, addr_fft;
    reg [0:87] string = 88'd0;
    reg [0:95] string_fft = 96'd0;
    
    always@(posedge pclk)
        if(!rst)
            begin
                char_code       <= string[addr+:7];
                char_code_fft   <= string_fft[addr_fft+:7];
            end
        else
            begin
                char_code       <= 7'd0;          
                char_code_fft   <= 7'd0;          
            end
            
    always@(*)
        begin
            case(period)
                T_05_S_DIV:
                    begin
                        string = "0.5[s/div]";
                        string_fft = "3.2[Hz/div]";
                    end     
                T_02_S_DIV:
                    begin
                        string = "0.2[s/div]";
                        string_fft = "8[Hz/div]";
                    end          
                T_01_S_DIV:
                    begin
                        string = "0.1[s/div]";
                        string_fft = "16[Hz/div]";
                    end          
                T_50_MS_DIV:
                    begin
                        string = "50[ms/div]";
                        string_fft = "32[Hz/div]";
                    end         
                T_20_MS_DIV:
                    begin
                        string = "20[ms/div]";
                        string_fft = "80[Hz/div]";
                    end         
                T_10_MS_DIV:
                    begin
                        string = "10[ms/div]";
                        string_fft = "160[Hz/div]";
                    end         
                T_5_MS_DIV:
                    begin
                        string = "5[ms/div]";
                        string_fft = "320[Hz/div]";
                    end          
                T_2_MS_DIV:
                    begin
                        string = "2[ms/div]";
                        string_fft = "800[Hz/div]";
                    end          
                T_1_MS_DIV:
                    begin
                        string = "1[ms/div]";
                        string_fft = "1.6[kHz/div]";
                    end          
                T_05_MS_DIV:
                    begin
                        string = "0.5[ms/div]";
                        string_fft = "3.2[kHz/div]";    
                    end         
                T_02_MS_DIV:
                    begin
                        string = "0.2[ms/div]";
                        string_fft = "8[kHz/div]";
                    end         
                T_01_MS_DIV:
                    begin
                        string = "0.1[ms/div]";
                        string_fft = "16[kHz/div]";
                    end             
                default:
                    begin
                        string = "1[ms/div]";
                        string_fft = "1.6[kHz/div]";
                    end                  
            endcase
        end
        
    assign addr_fft = {char_xy_fft, 3'b001};
    assign addr     = {char_xy, 3'b001};
    
endmodule
