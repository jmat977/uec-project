`timescale 1ns / 1ps

module data_mgmt(
    input wire clk130MHz,
    input wire rst,
    input wire new_frame,
    input wire m_axis_tvalid,
    input wire m_axis_data_tvalid,
    input wire [9:0] xk_index,
    input wire [9:0] ypos,
    input wire [11:0] adc_data_in,
    input wire [10:0] adc_mem_addr,
    input wire [10:0] fft_mem_addr,
    input wire [15:0] fft_data_in,
    output wire [8:0] trigger_lvl,
    output wire [15:0] adc_mem_sample,
    output wire [15:0] fft_mem_sample,
    output wire [17:0] fft_signals
    );
    
    wire [15:0] sample_out_tr;
    wire sample_enable, full;

    trigger my_trigger(
        .clk(clk130MHz),
        .rst(rst),
        .full(full),
        .ypos(ypos),       
        .new_frame(new_frame),
        .sample_in((adc_data_in[11]) ? {adc_data_in,4'b1111} : {adc_data_in, 4'b0000}),
        .interrupt(m_axis_tvalid),
        .sample_out(sample_out_tr),
        .sample_enable(sample_enable),
        .trigger_lvl_out(trigger_lvl));  
      
    wire [9:0] ram_addr;
    wire [15:0] value;
    wire output_enable;
      
    sample_mgmt my_sample_mgmt(
        .clk(clk130MHz),
        .rst(rst),
        .input_data(sample_out_tr),
        .input_enable(sample_enable),
        .output_enable(output_enable),
        .full(full),
        .ram_addr(ram_addr),
        .value(value));
    
    memory#(
        .DATA_WIDTH(16),
        .ADDR_WIDTH(10),
        .DEPTH(1024))
    my_adc_memory(                       
        .clk(clk130MHz),
        .write_enable(output_enable),
        .address_write(ram_addr),
        .address_read(adc_mem_addr[10] ? 10'd0 : adc_mem_addr[9:0]),                    
        .data_in(value),
        .data_out(adc_mem_sample)); 
        
    memory#(
        .DATA_WIDTH(16),
        .ADDR_WIDTH(8),
        .DEPTH(256))
    my_fft_memory(                           
        .clk(clk130MHz),
        .write_enable(m_axis_data_tvalid && !xk_index[9:8]),                            
        .address_write(xk_index[7:0]),
        .address_read(fft_mem_addr[10] ? 8'd0 : fft_mem_addr[9:2]),                     
        .data_in(fft_data_in),
        .data_out(fft_mem_sample)); 
        
    assign fft_signals = {value, output_enable, full};
        
endmodule
