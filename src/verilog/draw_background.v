`timescale 1ns / 1ps
`include "_vga_macros.vh"

module draw_background(
    input wire pclk,
    input wire rst,
    input wire [`VGA_BUS_SIZE_NO_RGB-1:0] vga_in,
    output wire [`VGA_BUS_SIZE-1:0] vga_out
);

    `VGA_SPLIT_INPUT_NO_RGB(vga_in)
    `VGA_OUT_REG
    `VGA_MERGE_OUTPUT(vga_out)

    localparam 
        BLANK       = 12'h0_0_0,
        GRID        = 12'h9_9_0,
        BACKGROUND  = 12'h0_0_0;
        
    reg [11:0] rgb_out_nxt = 12'd0;

    always @(posedge pclk)
        if(!rst)
            begin
                hcount_out  <= hcount_in;
                vcount_out  <= vcount_in;
                hsync_out   <= hsync_in;
                vsync_out   <= vsync_in;
                hblnk_out   <= hblnk_in;
                vblnk_out   <= vblnk_in;   
                rgb_out     <= rgb_out_nxt;
            end
        else                        
            begin
                hcount_out  <= 11'd0;
                vcount_out  <= 11'd0;
                hsync_out   <= 1'd0;
                vsync_out   <= 1'd0;
                hblnk_out   <= 1'd0;
                vblnk_out   <= 1'd0;
                rgb_out     <= 12'd0;
            end
    
    always @(*)
        begin
            if (vblnk_in || hblnk_in) rgb_out_nxt = BLANK; 
            else
                if(hcount_in == 101 || hcount_in == 204 || hcount_in == 306 || hcount_in == 409 
                || hcount_in == 613 || hcount_in == 716 || hcount_in == 818 || hcount_in == 921)        
                    rgb_out_nxt = GRID;
                else if (vcount_in == 127 || vcount_in == 383)   
                    rgb_out_nxt = GRID;
                else    
                    rgb_out_nxt = BACKGROUND; 
        end
endmodule
