`timescale 1ns / 1ps
`include "_vga_macros.vh"

module draw_rect_char#(parameter XPOS = 930, YPOS = 265, WIDTH = 88,HEIGHT = 16,COLOUR = 12'hF_F_F)(
    input wire pclk,
    input wire rst,
    input wire [7:0] char_pixels,
    input wire [`VGA_BUS_SIZE-1:0] vga_in,
    output wire [`VGA_BUS_SIZE-1:0] vga_out,
    output wire [3:0] char_xy,
    output wire [3:0] char_line
    );

    `VGA_SPLIT_INPUT(vga_in)
    `VGA_OUT_REG
    `VGA_MERGE_OUTPUT(vga_out)
    
    localparam 
        BLANK = 12'h0_0_0;
        
    reg [11:0] rgb_out_nxt = 12'd0;
    wire [10:0] hpos, vpos;
    
    always @(posedge pclk)
        if(!rst)
            begin
                rgb_out     <= rgb_out_nxt;
                hcount_out  <= hcount_in;
                vcount_out  <= vcount_in;
                hsync_out   <= hsync_in;
                vsync_out   <= vsync_in;
                hblnk_out   <= hblnk_in;
                vblnk_out   <= vblnk_in;               
            end
        else
            begin
                rgb_out     <= 12'd0;
                hcount_out  <= 11'd0;
                vcount_out  <= 11'd0;
                hsync_out   <= 1'd0;
                vsync_out   <= 1'd0;
                hblnk_out   <= 1'd0;
                vblnk_out   <= 1'd0;
            end  
   
    always @(*)
        begin
            if (vblnk_in || hblnk_in) rgb_out_nxt = BLANK; 
            else
                if(hcount_in < WIDTH + XPOS && vcount_in < HEIGHT + YPOS && hcount_in >= XPOS && vcount_in >= YPOS)
                    if(char_pixels[9 - hpos[2:0]])
                        rgb_out_nxt = COLOUR; 
                    else
                        rgb_out_nxt = rgb_in;
                else        
                    rgb_out_nxt = rgb_in;     
        end
                                                
    assign char_xy   = hpos[6:3];
    assign char_line = vpos[3:0]; 
    assign vpos = vcount_in - YPOS;
    assign hpos = hcount_in - XPOS + 2;
     
endmodule
