`timescale 1ps / 1ps
`include "_vga_macros.vh"

module draw_screen(
    input wire pclk,
    input wire rst,
    input wire [8:0] adc_data,    
    input wire [7:0] fft_bin_value,              
    input wire [`VGA_BUS_SIZE-1:0] vga_in,
    output wire [`VGA_BUS_SIZE-1:0] vga_out
);

    `VGA_SPLIT_INPUT(vga_in)
    `VGA_OUT_REG
    `VGA_MERGE_OUTPUT(vga_out)
    
    localparam 
        BLANK       = 12'h0_0_0,
        SIGNAL      = 12'h0_F_F,
        FFT         = 12'h9_1_B,
        THICKNESS   = 2;
        
    reg [11:0] rgb_out_nxt = 12'd0;
        
    always @(posedge pclk)
        if(!rst)
            begin
                hcount_out  <= hcount_in;
                vcount_out  <= vcount_in;
                hsync_out   <= hsync_in;
                vsync_out   <= vsync_in;
                hblnk_out   <= hblnk_in;
                vblnk_out   <= vblnk_in;
                rgb_out     <= rgb_out_nxt;
            end
        else
            begin
                hcount_out  <= 11'd0;
                vcount_out  <= 11'd0;
                hsync_out   <= 1'd0;
                vsync_out   <= 1'd0;
                hblnk_out   <= 1'd0;
                vblnk_out   <= 1'd0;
                rgb_out     <= 12'd0;
            end
            
    always @(*)
        begin
            if (vblnk_in || hblnk_in) rgb_out_nxt = BLANK; 
            else
                if ((vcount_in <= adc_data + THICKNESS) && (vcount_in >= adc_data - THICKNESS)) 
                    rgb_out_nxt = SIGNAL;
                else if ((vcount_in >= (766 - fft_bin_value)) && vcount_in != 767)  
                    rgb_out_nxt = FFT;                         
                else 
                    rgb_out_nxt = rgb_in; 
        end
        
endmodule
