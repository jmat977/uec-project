`timescale 1ns / 1ps

module interrupt_gen(
    input wire clk,
    input wire rst,
    input wire [11:0] period_select,
    output reg [19:0] period,
    output reg interrupt
    );
    
    localparam 
        T_05_S_DIV   = 20'd634766,
        T_02_S_DIV   = 20'd253906,
        T_01_S_DIV   = 20'd126953,
        T_50_MS_DIV  = 20'd63477,
        T_20_MS_DIV  = 20'd25391,
        T_10_MS_DIV  = 20'd12695,
        T_5_MS_DIV   = 20'd6348,
        T_2_MS_DIV   = 20'd2539,
        T_1_MS_DIV   = 20'd1269,
        T_05_MS_DIV  = 20'd635,
        T_02_MS_DIV  = 20'd254,
        T_01_MS_DIV  = 20'd127;
    
    reg [19:0] decum = 20'd0, decum_nxt = 20'd0;
    reg [19:0] period_nxt = 20'd0;
    reg interrupt_nxt = 1'b0;
    
    always @(posedge clk)  
        if(!rst)
            begin
                decum       <= decum_nxt;
                period      <= period_nxt;
                interrupt   <= interrupt_nxt;
            end
        else
            begin
                decum       <= 19'd0;
                period      <= 19'd0;
                interrupt   <= 1'd0;
            end
            
    always@(*)
        begin              
            case(period_select)
                12'b000000000001: period_nxt = T_05_S_DIV;   
                12'b000000000011: period_nxt = T_02_S_DIV;   
                12'b000000000111: period_nxt = T_01_S_DIV;   
                12'b000000001111: period_nxt = T_50_MS_DIV;  
                12'b000000011111: period_nxt = T_20_MS_DIV;  
                12'b000000111111: period_nxt = T_10_MS_DIV;  
                12'b000001111111: period_nxt = T_5_MS_DIV;   
                12'b000011111111: period_nxt = T_2_MS_DIV;   
                12'b000111111111: period_nxt = T_1_MS_DIV;   
                12'b001111111111: period_nxt = T_05_MS_DIV;  
                12'b011111111111: period_nxt = T_02_MS_DIV;  
                12'b111111111111: period_nxt = T_01_MS_DIV;  
                default: period_nxt = T_1_MS_DIV;            
            endcase
            decum_nxt = (interrupt ? period : decum) - 1;
            interrupt_nxt = (decum == 0);
        end
    
endmodule
