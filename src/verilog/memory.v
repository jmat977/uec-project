`timescale 1ns / 1ps

module memory #(parameter DATA_WIDTH = 16, ADDR_WIDTH = 10, DEPTH = 1024)(
    input wire  clk,
    input wire  write_enable,
    input wire  [ADDR_WIDTH-1:0] address_write,
    input wire  [ADDR_WIDTH-1:0] address_read,
    input wire  [DATA_WIDTH-1:0] data_in,
    output wire [DATA_WIDTH-1:0] data_out
);
     
    reg [DATA_WIDTH-1:0] ram[DEPTH-1:0];
        
    always @(posedge clk)
        if(write_enable)
            ram[address_write] <= data_in;
        else
            ram[address_write] <= ram[address_write];
        
    assign data_out = ram[address_read];

endmodule