`timescale 1ns / 1ps

module sample_mgmt(
    input wire clk,
    input wire rst,
    input wire [15:0] input_data,
    input wire input_enable,
    output reg output_enable,
    output reg [9:0] ram_addr,
    output reg [15:0] value,
    output reg full
    );
    
    localparam 
        CLEAR       = 2'b00,
        IDLE        = 2'b01,
        WRITE       = 2'b10,
        FRAME_SIZE  = 1024;
    
    reg [1:0] state = 2'd0, state_nxt = 2'd0;
    reg [15:0] value_nxt = 16'd0;
    reg [9:0] ram_addr_nxt = 10'd0, ram_addr_delay = 10'd0;
    reg output_enable_nxt = 1'b0, full_nxt = 1'b0;
    
    always @(posedge clk)
        if(!rst)
            begin
                ram_addr        <= ram_addr_delay;
                ram_addr_delay  <= ram_addr_nxt;
                value           <= value_nxt;
                full            <= full_nxt;
                output_enable   <= output_enable_nxt;
                state           <= state_nxt;
            end
        else
            begin
                ram_addr        <= 10'd0;
                ram_addr_delay  <= 10'd0;
                value           <= 16'd0;
                full            <= 1'd0;
                output_enable   <= 1'd0;
                state           <= CLEAR;
            end
    
    always@(state or input_enable or ram_addr_delay)
        begin
            case(state)
                IDLE:       state_nxt = input_enable ? WRITE : IDLE;
                WRITE:      state_nxt = IDLE;
                CLEAR:      state_nxt = (ram_addr_delay == FRAME_SIZE - 1) ? IDLE : CLEAR;
                default:    state_nxt = IDLE;
            endcase
        end

    always @(state or value or full or ram_addr_delay or output_enable or input_data)
        begin
            value_nxt         = value; 
            full_nxt          = full;
            ram_addr_nxt      = ram_addr_delay;
            output_enable_nxt = output_enable;
            case(state)
                IDLE:
                    begin
                        full_nxt = 0;
                        output_enable_nxt = 0;
                    end
                WRITE:
                    begin
                        value_nxt = input_data;
                        ram_addr_nxt = ram_addr_delay + 1;    
                        output_enable_nxt = 1; 
                        full_nxt = (ram_addr_delay == FRAME_SIZE - 1);   
                    end
                CLEAR:
                    begin
                        value_nxt = 0;
                        full_nxt = 0;
                        ram_addr_nxt = ram_addr_delay + 1;
                        output_enable_nxt = 1;
                    end
            endcase
        end
endmodule
