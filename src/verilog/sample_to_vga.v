`timescale 1ns / 1ps

module sample_to_vga(
    input wire clk,
    input wire rst,
    input wire signed [8:0] sample_in,
    output reg [8:0] sample_draw
    );
    
    localparam
        VERT_CENTER = 255;
    
    reg [8:0] sample_draw_nxt = 9'd0;
    
    always@(posedge clk)
        if(!rst)
            sample_draw <= sample_draw_nxt;
        else    
            sample_draw <= 9'd0;
            
    always@(*)
        sample_draw_nxt = VERT_CENTER - sample_in;
        
endmodule
