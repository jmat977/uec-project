`timescale 1ns / 1ps

module sample_to_vga_top(
    input wire clk65MHz,
    input wire rst,
    input wire [15:0] mem_sample,
    output wire [8:0] sample_draw
    );
    
    wire [15:0] mem_sample_del;
    
    delay#(
        .WIDTH(16),
        .CLK_DEL(1))
    my_sample_delay(
        .clk(clk65MHz),
        .rst(rst),
        .din(mem_sample),
        .dout(mem_sample_del));
        
    sample_to_vga my_sample_to_vga(
        .clk(clk65MHz),
        .rst(rst),
        .sample_in(mem_sample_del[15:7]),
        .sample_draw(sample_draw));
        
endmodule
