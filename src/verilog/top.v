`timescale 1 ps / 1 ps
`include "_vga_macros.vh"

module top(
    input wire clk,
    input wire rst,
    input wire new_frame,
    input wire new_frame_cont,
    input wire vauxp6,
    input wire vauxn6,
    inout wire ps2_clk,
    inout wire ps2_data,
    input wire [11:0] sw,
    output wire hs,
    output wire vs,
    output wire [3:0] r,
    output wire [3:0] g,
    output wire [3:0] b
);

    wire clk65MHz, clk130MHz, axis_rst;
    
    clk_wiz_0 my_clk_wizard(
        .clk(clk),
        .reset(rst),
        .clk65MHz(clk65MHz),
        .clk130MHz(clk130MHz),        
        .axis_rst(axis_rst));
        
    wire interrupt, m_axis_tvalid;
    wire [15:0] adc_data;
    wire [19:0] period;
    
    interrupt_gen my_interrupt_gen(
        .clk(clk130MHz),
        .rst(rst),
        .interrupt(interrupt),
        .period(period),
        .period_select(sw));
        
    xadc_wiz_0 my_xadc(
        .m_axis_aclk(clk130MHz),    
        .s_axis_aclk(clk130MHz),    
        .m_axis_resetn(axis_rst),
        .m_axis_tvalid(m_axis_tvalid),
        .m_axis_tready(1'b1),
        .m_axis_tdata(adc_data),  
        .m_axis_tid(),
        .convst_in(interrupt),
        .vp_in(1'b0),
        .vn_in(1'b0),
        .vauxp6(vauxp6),
        .vauxn6(vauxn6),
        .channel_out(),
        .eoc_out(),
        .alarm_out(),
        .eos_out(),
        .busy_out());

    wire [15:0] adc_mem_sample;    
    wire [8:0] trigger_lvl, sample_draw;
    wire [11:0] ypos;
    wire [`VGA_BUS_SIZE-1:0] vga_bus [1:0];
    
    wire m_axis_data_tvalid;
    wire [15:0] m_axis_data_tdata_re, m_axis_data_tdata_im;
    wire [15:0] xk_index;
    wire [17:0] fft_signals;
    
    xfft_0 my_xfft(
        .aclk(clk130MHz),                                                 
        .s_axis_config_tdata(16'd0),                                     
        .s_axis_config_tvalid(1'b0),                                      
        .s_axis_config_tready(),                                          
        .s_axis_data_tdata({16'd0, fft_signals[17:2]}),                   
        .s_axis_data_tvalid(fft_signals[1]),                              
        .s_axis_data_tready(),                                            
        .s_axis_data_tlast(fft_signals[0]),                               
        .m_axis_data_tdata({m_axis_data_tdata_im, m_axis_data_tdata_re}), 
        .m_axis_data_tuser(xk_index),                                     
        .m_axis_data_tvalid(m_axis_data_tvalid),                          
        .m_axis_data_tready(1'b1),                                        
        .m_axis_data_tlast(),                                             
        .event_frame_started(),                                           
        .event_tlast_unexpected(),                                        
        .event_tlast_missing(),                                           
        .event_status_channel_halt(),                                     
        .event_data_in_channel_halt(),                                    
        .event_data_out_channel_halt());                                 

    wire [15:0] fft_data_abs = m_axis_data_tdata_re[15] ? -m_axis_data_tdata_re : m_axis_data_tdata_re;
    wire [15:0] fft_bin_value;
    wire [10:0] fft_mem_addr;
    
    data_mgmt my_data_mgmt(
        .clk130MHz(clk130MHz),               
        .rst(rst),                     
        .new_frame(new_frame || new_frame_cont),               
        .m_axis_tvalid(m_axis_tvalid),           
        .m_axis_data_tvalid(m_axis_data_tvalid),      
        .xk_index(xk_index[9:0]),          
        .ypos(ypos[9:0]),              
        .adc_data_in(adc_data[15:4]),      
        .adc_mem_addr(vga_bus[0][10:0]), 
        .fft_mem_addr(fft_mem_addr),    
        .fft_data_in(fft_data_abs),      
        .trigger_lvl(trigger_lvl),      
        .adc_mem_sample(adc_mem_sample),  
        .fft_mem_sample(fft_bin_value),  
        .fft_signals(fft_signals));
        
    sample_to_vga_top my_sample_to_vga_top(
        .clk65MHz(clk65MHz),
        .rst(rst),
        .mem_sample(adc_mem_sample),
        .sample_draw(sample_draw));
        
    vga_top my_vga_top(
        .clk65MHz(clk65MHz),
        .rst(rst),
        .period(period),
        .vga_bus_out(vga_bus[0]));
        
    vga_drawing my_vga_drawing(
        .clk65MHz(clk65MHz),
        .rst(rst),
        .sample_draw(sample_draw),
        .fft_bin_value((fft_bin_value >= 8192) ? 8'b11111111 : fft_bin_value[12:5]),
        .trigger_lvl(trigger_lvl),
        .vga_bus_in(vga_bus[0]),
        .vga_bus_out(vga_bus[1]),
        .fft_mem_addr(fft_mem_addr));

    mouse_ctl my_mouse_ctl(
        .clk(clk65MHz),
        .rst(rst),
        .left(),
        .middle(),
        .right(),
        .ps2_clk(ps2_clk),
        .ps2_data(ps2_data),
        .new_event(),
        .setmax_x(1'b0),
        .setmax_y(1'b0),
        .setx(1'b0),
        .sety(1'b0),
        .value(12'h000),
        .xpos(),
        .ypos(ypos),
        .zpos());
        
    assign vs       = vga_bus[1][25];
    assign hs       = vga_bus[1][24];
    assign {r,g,b}  = vga_bus[1][37:26];
    
endmodule
