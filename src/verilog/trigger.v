`timescale 1ns / 1ps

module trigger(
    input wire clk,
    input wire rst,
    input wire signed [15:0] sample_in,
    input wire full,
    input wire interrupt,
    input wire new_frame,
    input wire [9:0] ypos,
    output wire [8:0] trigger_lvl_out,
    output reg [15:0] sample_out,
    output reg sample_enable
    );

    localparam 
        IDLE            = 2'b00,
        ACQUIRE         = 2'b01,
        WAIT            = 2'b10,
        VERT_CENTER     = 255,
        TRIGGER_CENTER  = 256,
        ADC_TO_TRIGGER  = 128;

    reg [1:0] state = 2'd0, state_nxt = 2'd0;
    reg threshold = 1'b0, threshold_nxt = 1'b0, sample_enable_nxt = 1'b0;
    wire signed [8:0] trigger_lvl;
    wire trigger;
    
    always @(posedge clk)
        if(!rst)
            begin
                state           <= state_nxt;
                threshold       <= threshold_nxt;
                sample_out      <= sample_in;
                sample_enable   <= sample_enable_nxt;
            end
        else
            begin
                state           <= IDLE;
                threshold       <= 1'b0;
                sample_out      <= 16'd0;
                sample_enable   <= 1'b0;
            end

    always @(state or trigger or full or new_frame)                     
        case(state)
            IDLE:       state_nxt = trigger ? ACQUIRE : IDLE;
            ACQUIRE:    state_nxt = full ? WAIT : ACQUIRE;
            WAIT:       state_nxt = new_frame ? IDLE : WAIT;
            default:    state_nxt = IDLE;
        endcase

    always @(state or interrupt)                                        
        case (state)
            IDLE:       sample_enable_nxt = 0;
            ACQUIRE:    sample_enable_nxt = interrupt;                  
            WAIT:       sample_enable_nxt = 0;
            default:    sample_enable_nxt = 0;
        endcase
    
    always @(*)
        threshold_nxt = ((sample_in / ADC_TO_TRIGGER) > trigger_lvl);
                  
    assign trigger = threshold_nxt & ~threshold;
    assign trigger_lvl = (~ypos / 2) - TRIGGER_CENTER;                    
    assign trigger_lvl_out = VERT_CENTER - trigger_lvl;
    
endmodule
