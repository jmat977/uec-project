`timescale 1ns / 1ps
`include "_vga_macros.vh"

module vga_drawing(
    input wire clk65MHz,
    input wire rst,
    input wire [8:0] sample_draw,
    input wire [8:0] trigger_lvl,
    input wire [7:0] fft_bin_value,
    input wire [`VGA_BUS_SIZE-1:0] vga_bus_in,
    output wire [`VGA_BUS_SIZE-1:0] vga_bus_out,
    output wire [10:0] fft_mem_addr
    );
    
    wire [`VGA_BUS_SIZE-1:0] vga_bus;
    wire [8:0] trigger_del;
    
    delay#(
        .WIDTH(9),
        .CLK_DEL(1))
    my_trigger_delay(
        .clk(clk65MHz),
        .rst(rst),
        .din(trigger_lvl),
        .dout(trigger_del));
    
    draw_trigger my_draw_trigger(
        .pclk(clk65MHz),        
        .rst(rst),
        .vga_in(vga_bus_in),
        .vga_out(vga_bus),
        .trigger_lvl(trigger_del));
        
    wire [7:0] fft_bin_value_del;
        
    delay#(
        .WIDTH(8),
        .CLK_DEL(1))
    my_fft_delay(
        .clk(clk65MHz),
        .rst(rst),
        .din(fft_bin_value),
        .dout(fft_bin_value_del));
        
    draw_screen my_draw_screen(
        .pclk(clk65MHz),
        .rst(rst),
        .vga_in(vga_bus),
        .vga_out(vga_bus_out),
        .adc_data(sample_draw),
        .fft_bin_value(fft_bin_value_del));
        
    assign fft_mem_addr = vga_bus[10:0];
    
endmodule
