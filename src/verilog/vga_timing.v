`timescale 1ns / 1ps
`include "_vga_macros.vh"

module vga_timing (
    input wire pclk,
    input wire rst,
    output wire [`VGA_BUS_SIZE_NO_RGB-1:0] vga_out
  );
  
    `VGA_OUT_REG_NO_RGB
    `VGA_MERGE_OUTPUT_NO_RGB(vga_out)
  
    localparam 
    H_VISIBLE       = 1024,
    H_FRONT_P       = 24,
    H_SYNC          = 136,
    H_BACK_P        = 160,
    H_TOTAL         = 1344,
    V_VISIBLE       = 768,
    V_FRONT_P       = 3,
    V_SYNC          = 6,
    V_BACK_P        = 29,
    V_TOTAL         = 806,
    H_SYNC_START    = H_VISIBLE + H_FRONT_P,
    H_SYNC_STOP     = H_TOTAL - H_BACK_P,
    V_SYNC_START    = V_VISIBLE + V_FRONT_P,
    V_SYNC_STOP     = V_TOTAL - V_BACK_P;

    reg vsync_nxt = 1'd0; 
    reg vblnk_nxt = 1'd0; 
    reg hsync_nxt = 1'd0; 
    reg hblnk_nxt = 1'd0; 
    reg [10:0] vcount_nxt = 11'd0;
    reg [10:0] hcount_nxt = 11'd0;
    
    always @(posedge pclk)
        if(!rst)
            begin
                vcount_out      <= vcount_nxt;
                hcount_out      <= hcount_nxt;
                vblnk_out       <= vblnk_nxt;
                vsync_out       <= vsync_nxt;
                hblnk_out       <= hblnk_nxt;
                hsync_out       <= hsync_nxt;
            end
        else                
            begin
                vcount_out      <= 11'd0;
                hcount_out      <= 11'd0;
                vblnk_out       <= 1'd0; 
                vsync_out       <= 1'd0; 
                hblnk_out       <= 1'd0; 
                hsync_out       <= 1'd0;
            end
            
    always @(*)
        begin
            if(hcount_out < H_TOTAL - 1)
                begin
                    hcount_nxt = hcount_out + 1;
                    vcount_nxt = vcount_out;
                end
            else
                begin
                    hcount_nxt = 0;
                    if(vcount_out < V_TOTAL)
                        vcount_nxt = vcount_out + 1;
                    else
                        vcount_nxt = 0;    
                end         
            vblnk_nxt = (vcount_nxt >= V_VISIBLE);
            vsync_nxt = (vcount_nxt >= V_SYNC_START && vcount_nxt <= V_SYNC_STOP);
            hblnk_nxt = (hcount_nxt >= H_VISIBLE);
            hsync_nxt = (hcount_nxt >= H_SYNC_START && hcount_nxt <= H_SYNC_STOP);
        end
            
endmodule

