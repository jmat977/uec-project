`timescale 1ns / 1ps
`include "_vga_macros.vh"

module vga_top(
    input wire clk65MHz,
    input wire rst,
    input wire [19:0] period,
    output wire [`VGA_BUS_SIZE-1:0] vga_bus_out
    );
    
    wire [19:0] period_del;
    wire [`VGA_BUS_SIZE-1:0] vga_bus [3:0];
    wire [`VGA_BUS_SIZE_NO_RGB-1:0] vga_timing; 
    
    delay#(
        .WIDTH(20),
        .CLK_DEL(1))
    my_interrupt_delay(
        .clk(clk65MHz),
        .rst(rst),
        .din(period),
        .dout(period_del));
    
    vga_timing my_timing(
        .pclk(clk65MHz),
        .rst(rst),
        .vga_out(vga_timing));
    
    draw_background my_background(
        .pclk(clk65MHz),
        .rst(rst),
        .vga_in(vga_timing),
        .vga_out(vga_bus[0]));
        
    draw_background_fft my_background_fft(
        .pclk(clk65MHz),
        .rst(rst),
        .vga_in(vga_bus[0]),
        .vga_out(vga_bus[1]));
        
        draw_fft_grid my_fft_grid(
        .pclk(clk65MHz),
        .rst(rst),
        .vga_in(vga_bus[1]),
        .vga_out(vga_bus[2]));
        
    wire [3:0] char_xy, char_xy_fft;  
    wire [3:0] char_line, char_line_fft;
    wire [6:0] char_code, char_code_fft;
    wire [7:0] font_char, font_char_fft;
    
    font_rom my_font_adc(
        .pclk(clk65MHz),
        .rst(rst),
        .addr({char_code, char_line}),
        .char_line_pixels(font_char));
        
    font_rom my_font_fft(
        .pclk(clk65MHz),
        .rst(rst),
        .addr({char_code_fft, char_line_fft}),
        .char_line_pixels(font_char_fft));
    
    draw_rect_char#(
        .XPOS(930),
        .YPOS(265), 
        .WIDTH(88),
        .HEIGHT(16),
        .COLOUR(12'hF_F_F)) 
    my_char_adc(
        .pclk(clk65MHz),
        .rst(rst),
        .vga_in(vga_bus[2]),
        .vga_out(vga_bus[3]),
        .char_pixels(font_char),
        .char_xy(char_xy),
        .char_line(char_line));
    
    draw_rect_char#(
        .XPOS(4),
        .YPOS(488), 
        .WIDTH(96),
        .HEIGHT(16),
        .COLOUR(12'hF_8_0)) 
    my_char_fft(
        .pclk(clk65MHz),
        .rst(rst),
        .vga_in(vga_bus[3]),
        .vga_out(vga_bus_out),
        .char_pixels(font_char_fft),
        .char_xy(char_xy_fft),
        .char_line(char_line_fft));
    
    char_rom my_char_rom(
        .pclk(clk65MHz),
        .rst(rst),
        .period(period_del),
        .char_xy(char_xy),
        .char_xy_fft(char_xy_fft),
        .char_code(char_code),
        .char_code_fft(char_code_fft));
    
endmodule
